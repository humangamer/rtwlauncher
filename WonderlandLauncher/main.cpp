#include <Windows.h>
#include <time.h>

#include "resource.h"

//#define SHIWORD(x) signed __int16(x)
#define SHIWORD(x) signed __int16 HIWORD(x)

int soundVolume = 100;
int musicVolume = 100;
int detailLevel = 3;
int gameResolution = 1;
int gameBitDepth = 1;
int editorBitDepth = 1;
int gameWindowed = 0;
int language = 0;
int noTimeLimit = 0;
int lockWorlds = 0;
int teleportStyle = 0;
int editorWindowed = 0;
const char* a640x480 = "640x480";
const char* a800x600 = "800x600";
const char* a1024x768 = "1024x768";
const char* a1280x960 = "1280x960";
const char* a1280x1024 = "1280x1024";
const char* a16Bit = "16 Bit";
const char* a24Bit = "24 Bit";
const char* a32Bit = "32 Bit";
const char* aFullScreen = "Full Screen";
const char* aWindowed = "Windowed";
const char* aLdr2_dat = "ldr2.dat";
const char* aLdr1_dat = "ldr1.dat";
const char* pszSound = "ldr3.dat";
const char* ClassName = "GDI01";
const char* aWdlgamep_exe = "wdlgamep.exe";
const char* aWdleditp_exe = "wdleditp.exe";
const char* Operation = "open";
const char* File = "http://www.midnightsynergy.com/returntowonderland/levelex/index.html";
const char* FileName = "options.cfg";
const char* WindowName = "RTW Loader";
HBITMAP bgImage;
time_t time1;
time_t time2;
int type;

void loadBitmap(HWND hWnd)
{
	HINSTANCE bitmap = (HINSTANCE)GetWindowLongA(hWnd, GWL_HINSTANCE);
	bgImage = LoadBitmap(bitmap, MAKEINTRESOURCE(IDB_BACKGROUND));
}

BOOL draw(HWND hWnd)
{
	int v2; // eax@28
	int v3; // eax@29
	int v4; // eax@43
	int v5; // eax@44
	const CHAR *v7; // [sp-10h] [bp-90h]@5
	const CHAR *v8; // [sp-10h] [bp-90h]@19
	const CHAR *v9; // [sp-10h] [bp-90h]@34
	int v10; // [sp-Ch] [bp-8Ch]@5
	int v11; // [sp-Ch] [bp-8Ch]@19
	int v12; // [sp-Ch] [bp-8Ch]@34
	struct tagRECT *v13; // [sp-8h] [bp-88h]@5
	struct tagRECT *v14; // [sp-8h] [bp-88h]@19
	struct tagRECT *v15; // [sp-8h] [bp-88h]@34
	HGDIOBJ img; // [sp+10h] [bp-70h]@1
	HDC hdc; // [sp+14h] [bp-6Ch]@1
	struct tagRECT rc; // [sp+18h] [bp-68h]@1
	int cx = 320; // [sp+2Ch] [bp-54h]@1
	int cy = 300; // [sp+30h] [bp-50h]@1
	struct tagPAINTSTRUCT Paint; // [sp+40h] [bp-40h]@1

	HDC winPaint = BeginPaint(hWnd, &Paint);
	hdc = CreateCompatibleDC(0);
	img = SelectObject(hdc, bgImage);
	void* pv = malloc(24);
	GetObject(bgImage, 24, pv);
	BitBlt(winPaint, 0, 0, cx, cy, hdc, 0, 0, SRCCOPY);
	rc.left = 8;
	rc.top = 130;
	rc.right = 143;
	rc.bottom = 191;
	if (gameResolution < 0 || gameResolution > 4)
	{
		gameResolution = 0;
	LABEL_13:
		v13 = &rc;
		v10 = lstrlenA(a640x480);
		v7 = a640x480;
		goto LABEL_14;
	}
	if (gameResolution == 1)
	{
		v13 = &rc;
		v10 = lstrlenA(a800x600);
		v7 = a800x600;
	LABEL_14:
		DrawTextA(winPaint, v7, v10, v13, 1u);
		goto LABEL_15;
	}
	if (gameResolution == 2)
	{
		v13 = &rc;
		v10 = lstrlenA(a1024x768);
		v7 = a1024x768;
		goto LABEL_14;
	}
	if (gameResolution == 3)
	{
		v13 = &rc;
		v10 = lstrlenA(a1280x960);
		v7 = a1280x960;
		goto LABEL_14;
	}
	if (gameResolution == 4)
	{
		v13 = &rc;
		v10 = lstrlenA(a1280x1024);
		v7 = a1280x1024;
		goto LABEL_14;
	}
	if (!gameResolution)
		goto LABEL_13;
LABEL_15:
	rc.left = 8;
	rc.top = 150;
	rc.right = 143;
	rc.bottom = 191;
	if (gameBitDepth < 1 || gameBitDepth > 4)
	{
		gameBitDepth = 1;
	LABEL_19:
		v14 = &rc;
		v11 = lstrlenA(a16Bit);
		v8 = a16Bit;
		goto LABEL_24;
	}
	if (gameBitDepth == 1)
		goto LABEL_19;
	if (gameBitDepth == 2)
	{
		v14 = &rc;
		v11 = lstrlenA(a24Bit);
		v8 = a24Bit;
	}
	else
	{
		if (gameBitDepth != 3)
			goto LABEL_25;
		v14 = &rc;
		v11 = lstrlenA(a32Bit);
		v8 = a32Bit;
	}
LABEL_24:
	DrawTextA(winPaint, v8, v11, v14, 1u);
LABEL_25:
	rc.left = 8;
	rc.top = 170;
	rc.right = 143;
	rc.bottom = 191;
	if (gameWindowed)
	{
		if (gameWindowed == 1)
		{
		LABEL_29:
			v3 = lstrlenA(aWindowed);
			DrawTextA(winPaint, aWindowed, v3, &rc, 1u);
			goto LABEL_30;
		}
		gameWindowed = 0;
	}
	v2 = lstrlenA(aFullScreen);
	DrawTextA(winPaint, aFullScreen, v2, &rc, 1u);
	if (gameWindowed == 1)
		goto LABEL_29;
LABEL_30:
	rc.left = 174;
	rc.top = 130;
	rc.right = 309;
	rc.bottom = 191;
	if (editorBitDepth < 1 || editorBitDepth > 4)
	{
		editorBitDepth = 1;
	LABEL_34:
		v15 = &rc;
		v12 = lstrlenA(a16Bit);
		v9 = a16Bit;
		goto LABEL_39;
	}
	if (editorBitDepth == 1)
		goto LABEL_34;
	if (editorBitDepth == 2)
	{
		v15 = &rc;
		v12 = lstrlenA(a24Bit);
		v9 = a24Bit;
	}
	else
	{
		if (editorBitDepth != 3)
			goto LABEL_40;
		v15 = &rc;
		v12 = lstrlenA(a32Bit);
		v9 = a32Bit;
	}
LABEL_39:
	DrawTextA(winPaint, v9, v12, v15, 1u);
LABEL_40:
	rc.left = 174;
	rc.top = 150;
	rc.right = 309;
	rc.bottom = 191;
	if (editorWindowed)
	{
		if (editorWindowed == 1)
		{
		LABEL_44:
			v5 = lstrlenA(aWindowed);
			DrawTextA(winPaint, aWindowed, v5, &rc, 1u);
			goto LABEL_45;
		}
		editorWindowed = 0;
	}
	v4 = lstrlenA(aFullScreen);
	DrawTextA(winPaint, aFullScreen, v4, &rc, 1u);
	if (editorWindowed == 1)
		goto LABEL_44;
LABEL_45:
	SelectObject(hdc, img);
	DeleteDC(hdc);
	return EndPaint(hWnd, &Paint);
}

int __stdcall windowProc(HWND hWnd, int Msg, WPARAM wParam, LPARAM lParam)
{
	//__int16 v9; // cx@19

	if ((unsigned int)Msg <= 0x100)
	{
		if (Msg == WM_KEYFIRST)
		{
			if (wParam == 27)
			{
				time(&time1);
				PostQuitMessage(0);
				PlaySoundA(aLdr1_dat, 0, 0x20001u);
				DeleteObject(bgImage);
				return DefWindowProcA(hWnd, WM_KEYFIRST, 0x1Bu, lParam);
			}
		}
		else
		{
			if (Msg == WM_CREATE)
			{
				loadBitmap(hWnd);
				PlaySoundA(pszSound, 0, 0x20001u);
				return DefWindowProcA(hWnd, 1u, wParam, lParam);
			}
			if (Msg == WM_PAINT)
			{
				draw(hWnd);
				return DefWindowProcA(hWnd, Msg, wParam, lParam);
			}
		}
		return DefWindowProcA(hWnd, Msg, wParam, lParam);
	}
	if (Msg != WM_LBUTTONDOWN)
		return DefWindowProcA(hWnd, Msg, wParam, lParam);

	int xPos = LOWORD(lParam);
	int yPos = HIWORD(lParam);
	if (xPos > 8)
	{
		if (xPos < 143 && yPos > 130 && yPos < 150)
		{
			gameResolution++;
			if (gameResolution > 5)
				gameResolution = 1;
			RedrawWindow(hWnd, 0, 0, 0x105u);
			PostMessageA(hWnd, 0xFu, 0, 0);
			PlaySoundA(aLdr1_dat, 0, 0x20001u);
		}
		if (xPos > 8)
		{
			if (xPos < 143 && yPos > 150 && yPos < 170)
			{
				gameBitDepth++;
				if (gameBitDepth > 3)
					gameBitDepth = 1;
				RedrawWindow(hWnd, 0, 0, 0x105u);
				PostMessageA(hWnd, 0xFu, 0, 0);
				PlaySoundA(aLdr1_dat, 0, 0x20001u);
			}
			if (xPos <= 8 || xPos >= 143 || yPos <= 170 || yPos >= 190)
				goto LABEL_31;
			gameWindowed = 1 - gameWindowed;
			RedrawWindow(hWnd, 0, 0, 0x105u);
			PostMessageA(hWnd, 0xFu, 0, 0);
			PlaySoundA(aLdr1_dat, 0, 0x20001u);
		}
	}
LABEL_31:
	if (xPos > 174)
	{
		if (xPos < 309 && yPos > 130 && yPos < 150)
		{
			editorBitDepth++;
			if (editorBitDepth > 3)
				editorBitDepth = 1;
			RedrawWindow(hWnd, 0, 0, 0x105u);
			PostMessageA(hWnd, 0xFu, 0, 0);
			PlaySoundA(aLdr1_dat, 0, 0x20001u);
		}
		if (xPos > 174 && xPos < 309 && yPos > 150 && yPos < 170)
		{
			editorWindowed = 1 - editorWindowed;
			RedrawWindow(hWnd, 0, 0, 0x105u);
			PostMessageA(hWnd, 0xFu, 0, 0);
			PlaySoundA(aLdr1_dat, 0, 0x20001u);
		}
	}
	if (xPos > 7 && xPos < 146 && yPos > 209 && yPos < 248)
	{
		type = 1;
		time(&time1);
		PlaySoundA(aLdr2_dat, 0, 0x20001u);
		PostQuitMessage(0);
	}
	if (xPos > 173 && xPos < 312 && yPos > 209 && yPos < 248)
	{
		type = 2;
		time(&time1);
		PlaySoundA(aLdr2_dat, 0, 0x20001u);
		PostQuitMessage(0);
	}
	if (xPos > 90 && xPos < 230 && yPos > 253 && yPos < 292)
	{
		type = 3;
		time(&time1);
		PlaySoundA(aLdr2_dat, 0, 0x20001u);
		PostQuitMessage(0);
	}
	return 1;
}

void createWindowClass(HINSTANCE hInst)
{
	WNDCLASSEXA wndClass; // [sp+4h] [bp-30h]@1

	wndClass.cbSize = 48;
	wndClass.style = 3;
	wndClass.lpfnWndProc = (WNDPROC)windowProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInst;
	wndClass.hIcon = LoadIconA(0, (LPCSTR)0x7F00);
	wndClass.hCursor = LoadCursorA(0, (LPCSTR)0x7F00);
	wndClass.hbrBackground = (HBRUSH)6;
	wndClass.lpszMenuName = 0;
	wndClass.lpszClassName = ClassName;
	wndClass.hIconSm = LoadIconA(0, (LPCSTR)0x7F00);
	RegisterClassExA(&wndClass);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	int v5; // ST14_4@1
	int v6; // eax@1
	HWND hwnd; // ebx@1
	HANDLE v8; // eax@1
	void *v9; // esi@1
	HANDLE v10; // eax@6
	void *v11; // edi@6
	HANDLE v12; // eax@14
	void *v13; // edi@14
	HANDLE v14; // edi@22
	struct tagMSG Msg; // [sp+10h] [bp-1Ch]@3

	createWindowClass(hInstance);
	v5 = (GetSystemMetrics(1) - 300) / 2;
	v6 = GetSystemMetrics(0);
	hwnd = CreateWindowExA(0, ClassName, WindowName, 0x90800000, (v6 - 320) / 2, v5, 320, 300, 0, 0, hInstance, 0);
	ShowWindow(hwnd, nShowCmd);
	UpdateWindow(hwnd);
	v8 = CreateFileA(FileName, 0x80000000, 0, 0, 3u, 0x80u, 0);
	v9 = v8;
	if (v8 != (HANDLE)-1)
	{
		ReadFile(v8, &soundVolume, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &musicVolume, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &detailLevel, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &gameResolution, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &gameBitDepth, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &gameWindowed, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &language, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &noTimeLimit, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &lockWorlds, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &editorBitDepth, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &editorWindowed, 4u, (LPDWORD)&hInstance, 0);
		ReadFile(v9, &teleportStyle, 4u, (LPDWORD)&hInstance, 0);
	}
	CloseHandle(v9);
	RedrawWindow(hwnd, 0, 0, 0x105u);
	PostMessageA(hwnd, 0xFu, 0, 0);
	while (GetMessageA(&Msg, 0, 0, 0))
	{
		TranslateMessage(&Msg);
		DispatchMessageA(&Msg);
	}
	if (type == 1)
	{
		v10 = CreateFileA(FileName, 0x40000000u, 0, 0, 2u, 0x80u, 0);
		v11 = v10;
		if (v10 != (HANDLE)-1)
		{
			WriteFile(v10, &soundVolume, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &musicVolume, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &detailLevel, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &gameResolution, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &gameBitDepth, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &gameWindowed, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &language, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &noTimeLimit, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &lockWorlds, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &editorBitDepth, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &editorWindowed, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v11, &teleportStyle, 4u, (LPDWORD)&hInstance, 0);
		}
		CloseHandle(v11);
		if (!time1)
			time(&time1);
		time(&time2);
		while (time2 - time1 < 2)
			time(&time2);
		ShellExecuteA(0, Operation, File, 0, 0, 1);
	}
	if (type == 2)
	{
		v12 = CreateFileA(FileName, 0x40000000u, 0, 0, 2u, 0x80u, 0);
		v13 = v12;
		if (v12 != (HANDLE)INVALID_HANDLE_VALUE)
		{
			WriteFile(v12, &soundVolume, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &musicVolume, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &detailLevel, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &gameResolution, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &gameBitDepth, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &gameWindowed, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &language, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &noTimeLimit, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &lockWorlds, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &editorBitDepth, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &editorWindowed, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v13, &teleportStyle, 4u, (LPDWORD)&hInstance, 0);
		}
		CloseHandle(v13);
		if (!time1)
			time(&time1);
		time(&time2);
		while (time2 - time1 < 2)
			time(&time2);
		ShellExecuteA(0, Operation, aWdleditp_exe, 0, 0, 1);
	}
	if (type == 3)
	{
		v14 = CreateFileA(FileName, 0x40000000u, 0, 0, 2u, 0x80u, 0);
		if (v14 != (HANDLE)-1)
		{
			WriteFile(v14, &soundVolume, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &musicVolume, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &detailLevel, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &gameResolution, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &gameBitDepth, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &gameWindowed, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &language, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &noTimeLimit, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &lockWorlds, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &editorBitDepth, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &editorWindowed, 4u, (LPDWORD)&hInstance, 0);
			WriteFile(v14, &teleportStyle, 4u, (LPDWORD)&hInstance, 0);
		}
		CloseHandle(v14);
		if (!time1)
			time(&time1);
		time(&time2);
		while (time2 - time1 < 2)
			time(&time2);
		ShellExecuteA(0, Operation, aWdlgamep_exe, 0, 0, 1);
	}
	return Msg.wParam;
}